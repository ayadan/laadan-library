# Láadan Library

![Artist's Wife with a Book](https://github.com/Aya-Dan/Laadan-Library/blob/nede/images/georg_friedrich_schmidt_1752_The_Artists_Wife_with_a_Book.jpg?raw=true)

A place to read, post, and maintain stories written in Láadan

## Directory

* original
    * short-stories
        * Amberwind Stories
            * [AberwindStories.pdf](original/short-stories/Amberwind%20Stories/AmberwindStories.pdf)
    * songs
        * Birthsong
            * [Birthsong.mp3](https://github.com/Aya-Dan/Laadan-Library/blob/nede/original/songs/Birthsong/Birthsong.mp3?raw=true)
            * [Birthsong.pdf](original/songs/Birthsong/Birthsong.pdf)
            * [Birthsong in MuseScore](https://github.com/Aya-Dan/Laadan-Library/blob/nede/original/songs/Birthsong/Birthsong.mscz?raw=true)
* translations
    * short-stories
        * Aesops-Fables
            * [DedideAesopetho.pdf](translations/short-stories/Aesops-Fables/DedideAesopetho.pdf)

## How to contribute

### Register an account

To be able to upload your own work, you will need a GitHub account.
You can register one for free - since the repository is public,
it is free to host our work here.

### Creating a "fork"

You will have your own repositories in your own account, but you can
make copies of other repositories. Once you're done making changes in
*your* version of the repository, then you can request that we merge
your changes into the original one.

In the upper-right of the main [Laadan-Library repository page](https://github.com/Aya-Dan/Laadan-Library),
you will click on the **Fork** button. It will go to a loading screen, and then
redirect you to your copy. You can tell you're at your copy, because it
will say your username up top, with a link to the original right below it.

![Fork button](https://github.com/Aya-Dan/Laadan-Library/blob/nede/images/fork.png?raw=true)

### Adding files

There are two ways you can add files to the repository - you can either upload
files off your computer, or you can use the web-based text-editor to write something directly into your repository.

![Add and upload buttons](https://github.com/Aya-Dan/Laadan-Library/blob/nede/images/createupload.png?raw=true)

### Requesting to merge back with the original

When you've made changes and you are ready to request an addition to the original repository,
click on **Pull requests**. From this page, click **New pull request**

It will give you an overview of what changes you've made.
Click the green **Create pull request** button to submit your request.



### Questions?

If you need help or have questions, please email Rachel@Moosader.com .

## Links

* [Láadan quick-search dictionary](http://ayadan.moosader.com/gadgets/laadan-dictionary/)
* [Láadan WikiBook](https://en.wikibooks.org/wiki/L%C3%A1adan)
* [Additional Láadan resources at Áya Dan](http://ayadan.moosader.com/laadan/)
