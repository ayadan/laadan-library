# Om omá - Teacher teaches

Song in Láadan by Rachel Wil Sha Singh

*I've never written songs before plz be nice*

## Chords

Verse: F G Am F

Chorus: Am F G Am

Bridge: F Am F Am

![Ukulele tab](ukulele.png)

U D U D U D

## Lyrics

Verse 1 (student):

* Sháad le ulinedi
* Ulanin le bediwan.
* Methi bedihá loleth. 
* Menáwí len omáwáan.

* I go to school,
* I study to learn.
* Students have togetherness,
* We grow because of the teacher.

Chorus: 

* Om omá, izh báa bedi be bebáath?
* Ban be beyóoth bedihádi.
* Om omá, izh báa thel be bebáath?
* Náwí ra be, láad be lalith oyanan.

* The teacher teaches, but what does she learn?
* She gives herself to the students.
* The teacher teaches, but what does she receive?
* She doesn't grow, she feels no rain on her skin.


Verse 2 (college): 
 
* Medul ra len nedi.
* Dush náhalehule ne.
* Methi ra len losheth.
* Shulin hal nethe.

* We won't give you rest.
* You must continue to work to an intolerable degree.
* We don't have money.
* Your (for an inexplicable reason) work overflows.

Chorus:

* Om omá, izh báa bedi be bebáath?
* Ban be beyóoth bedihádi.
* Om omá, izh báa thel be bebáath?
* Náwí ra be, láad be lalith oyanan.

* The teacher teaches, but what does she learn?
* She gives herself to the students.
* The teacher teaches, but what does she receive?
* She doesn't grow, she feels no rain on her skin.

Bridge (teacher):

* Bóoth ban ledi duleth.
* Bíith rani hal lethe.

* In pain, I request: Give me a rest.
* In pain, I say: My work is a hollow accomplishment.

Chorus: 

* Om omá, izh báa bedi be bebáath?
* Ban be beyóoth bedihádi.
* Om omá, izh báa thel be bebáath?
* Náwí ra be, láad be lalith oyanan.

* The teacher teaches, but what does she learn?
* She gives herself to the students.
* The teacher teaches, but what does she receive?
* She doesn't grow, she feels no rain on her skin.







## Words

* them = need, need for
* néde = to want
* dush = to have to
* thi = to have
* om = to teach
* náwí = to grow
* raban = to take away from
* hahodib = deliberately shut off from feelings
* aálá = to spray (with water)
* lali = to rain
* sholalan = alone in a crowd of people
* shóoban = to enable, to help make happen
* shulin = to overflow
