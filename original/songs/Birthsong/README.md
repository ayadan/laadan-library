# Birthsong

## Credit

Published in A First Dictionary and Grammar of Láadan, by Suzette Haden Elgin

The song "House of the Rising Sun" is by The Animals, 1964

Sheet music adapted from https://www.8notes.com/scores/13845.asp

Adapted by Rachel Wil Sha Singh

## Original

![Birthsong](birthsong.jpg)

## Sheet music

![Sheet music](Birthsong-1.png)

![Sheet music](Birthsong-2.png)

## Audio

[Birthsong.mp3](Birthsong.mp3)
