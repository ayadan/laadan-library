# English: The Crow and the Pitcher

A Crow, half-dead with thirst, came upon a Pitcher which had
once been full of water; but when the Crow put its beak into the
mouth of the Pitcher he found that only very little water was left
in it, and that he could not reach far enough down to get at it.

He tried, and he tried, but at last had to give up in despair.
Then a thought came to him, and he took a pebble and dropped it
into the Pitcher.

Then he took another pebble and dropped it into
the Pitcher.  Then he took another pebble and dropped that into
the Pitcher.  Then he took another pebble and dropped that into
the Pitcher.  Then he took another pebble and dropped that into
the Pitcher.  Then he took another pebble and dropped that into
the Pitcher.

At last, at last, he saw the water mount up near
him, and after casting in a few more pebbles he was able to quench
his thirst and save his life.

*Little by little does the trick.*


# Láadan: ?


# Ido: La korniko e la krucho

**Translation by Tea Coba**

Korniko, qua esis preske morta pro dursto, arivis a krucho qua olim esis plena de aquo, ma kande la korniko pozis sua beko en la kolo del krucho, il deskovris ke nur poka aquo restis, e ke il ne povas extendar su por atingar ol. Il esforcis ed esforcis, ma fine il mustis renuncar desespere.

Pose, ideo venis en sua kapo, il prenis stoneto e lasis falar to en la krucho. Seque il prenis altra stoneto e lasis falar to en la krucho. Seque il prenis altra stoneto e lasis falar to en la krucho. Seque il prenis altra stoneto e lasis falar to en la krucho.

Seque il prenis altra stoneto e lasis falar to en la krucho. 
Seque il prenis altra stoneto e lasis falar to en la krucho.

Fine, il vidis la aquo elevar e pos insertar poka stoneti plu il esis kapabla extingar sua dursto e salvar sua vivo.


*La suceson on atingas pokope.*



---

## Translation notes

### Word list


