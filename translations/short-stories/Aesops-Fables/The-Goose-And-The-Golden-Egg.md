# English: The Fox and the Crow

There was once a Countryman who possessed the most wonderful Goose you can imagine, for every day when he visited the nest, the Goose had laid a beautiful, glittering, golden egg.

The Countryman took the eggs to market and soon began to get rich. But it was not long before he grew impatient with the Goose because she gave him only a single golden egg a day. He was not getting rich fast enough.

Then one day, after he had finished counting his money, the idea came to him that he could get all the golden eggs at once by killing the Goose and cutting it open. But when the deed was done, not a single golden egg did he find, and his precious Goose was dead.

*Those who have plenty want more and so lose all they have.*



# Láadan: ?


# Ido: La ganso e la ora ovo

**Translation by Tea Coba**

Olim esis rurano qua posedis la maxim marveloza ganso qua on povas imaginar, nam omnadie kande il vizitis la nesto, la ganso depozis bela, brileganta, ora ovo

La rurano adportis l’ovi a la merkato e quik richeskis. Sed balde la rurano perdis la pacienteso pro ke la ganso nur depozis unu ora ovo die. Il ne esis richeskanta sat rapide.

Ulatempe, pos il kalkulis ilua pekunio, il pensis ke il povus obtenar omna ora ovi apertante ed ocidante la ganso. Ma pos il facis to, nulan oran ovon trovis il e ilua miniona ganso esis morta.

*Lu qui havas abundo e volas plu perdas omno quo lu havas.*



---

## Translation notes

### Word list


