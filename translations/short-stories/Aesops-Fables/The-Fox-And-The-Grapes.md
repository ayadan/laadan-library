# English: The Fox and the Grapes

One afternoon a fox was walking through the forest and spotted a bunch of grapes hanging from over a lofty branch. "Just the thing to quench my thirst," said he.

Taking a few steps back, the fox jumped and just missed the hanging grapes. Again the fox took a few steps back, ran, jumped, and tried to reach them but still failed.

Finally giving up, the fox turned up his nose and said, "They're probably sour anyway," and proceeded to walk away.

*It is easy to despise what you cannot have.*

# Láadan: Dumidal i éelen


# Ido: La foxo e la vito-beri

**Translation by Tea Coba**

Olim vespere foxo esis promenanta tra la foresto e remarkis vito-grapo qua pendis de altega brancho. "Yen quon me bezonas por extingar mea dursto." Il dicis.

Retroirante kelke pazi, la foxo saltis e faliis la pendanta vito-beri. Itere, la foxo retroiris kelke pazi, kuris, saltis, e sensucese probis atingar oli.

La fine renuncanta foxo regardis la vito-beri desestime e dicis, "Probable oli esas acerba" e komencis livar.

*To, quon on ne povas havar esas facile desprizebla.*


---

## Translation notes

### Word list

* Fox - dumidal
* Grape - éelen
* Grapevine - éelenethil
* Vine - thil
* To jump - oób
* To perceive with the eyes - láad oyinan
* Sour - yem
