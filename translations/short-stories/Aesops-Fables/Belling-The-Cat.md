# English: Belling the Cat

The Mice once called a meeting to decide on a plan to free themselves of their enemy, the Cat.
At least they wished to find some way of knowing when she was coming, so they might have time to run away.
Indeed, something had to be done, for they lived in such constant fear of her claws that they hardly dared
stir from their dens by night or day.

Many plans were discussed, but none of them was thought good enough.
At last a very young Mouse got up and said:

"I have a plan that seems very simple, but I know it will be successful.
All we have to do is to hang a bell about the Cat's neck. When we hear the bell
ringing we will know immediately that our enemy is coming."

All the Mice were much surprised that they had not thought of such a plan before.
But in the midst of the rejoicing over their good fortune, an old Mouse arose and said:

"I will say that the plan of the young Mouse is very good. But let me ask one question:
Who will bell the Cat?"

*It is one thing to say that something should be done, but quite a different matter to do it.*


# Láadan: Limlim ruletho (Cat's bell)

Láadan:

1. Bíide eril mehabelid edemid wo. Íi Habelid rul.
2. Mehéeya edemid ruleth. Methad ra mesháad edemid maradalede.
3. Mebithim edemid. Mime wohuná wohedemid, "Báa merumad len?"
4. Melith i medi edemid, izh, thi rawith wothal wolith.
5. Doól, di háahedemid, "Bíi thi le woradimil *wowodal* wa."
6. "Medush len **dóhune rul limlimethehé**.
7. Bé aril methad meláad len ruleth láad oyunan."
8. Melo edemid, izh, di wobalin wohedemid,
9. "Bíi thi wodal wa! Izh: Báa aril hal bebáa?"

10. Bíi híya wodal, rahíya wilomina.

English:
 
1. Once, in the past, there were mice that lived in a house. A cat also lived in the house (this is a made up story).
2. The mice feared the cat. The mice could not leave from their hole.
3. The mice met. The leader mouse asked, "How can we hide?"
4. The mice thought and talked, but, nobody had a good thought.
5. Finally, a mouse child said, "I have a simple idea."
6. "We have to make the cat wear a bell.
7. I promise that we will be able to hear the cat."
8. The mice rejoiced, however, the old mouse said,
9. "It is a good idea, but: Who will do the work?"

10. Ideas are small, to perform is big.



**Notes**

I didn't write every line in the past-tense (with "eril"), because I don't
like the redundancy of the same word over and over. Hopefully it is clear from context.

Line 6 is tricky, using the embedded sentence lesson: https://en.wikibooks.org/wiki/L%C3%A1adan/Lessons/19




# Ido: La sonalizo del kato

**Translation by Tea Coba**

La musi olim asembleskis por propozar quale liberar su del sua enemiko, la kato. Oli deziris ad minime trovar maniero por anuncar elua veno, tale oli havus tempo por eskapar. Certe, ulo esis facenda, oli tale konstante timegis elua onglegi ke oli apene audacis movar ek sua kusheyi die o nokte.
Multa skisuri esis diskutita, ma nula esis  sat bona. Fine, yuna muso levis su e dicis:

"Me havas skisuro qua semblas tre simpla, ma me savas ke ol esos tre sucesoza.
Omnon quon ni mustas facar esas pendigar sonalio cirkum la kolo del kato. Kande ni audos la sonalio sonar, ni quik savos ke nia enemiko venas.

Omnu musi esis surprisita pro la fakto ke oli ne havis tala ideo. Ma dum oli joyis pri sua bona fortuno, ulu olda muso leveskis e dicis:

"Me opinionas ke la skisuro del yuna muso estas tre bona.. Ma lasez me demandar ulo: Qua sonalizos la kato?"

*Esas plu facila dicar ke ulo esas facenda ol facar to.*



---

## Translation notes

### Word list

* edemid = mouse
* limlim = bell
* zho = sound (audible)
* láad oyunan = to perceive with the ears
* bithim = meeting
* eth = to be about (as in a book about birds)
* habelid = to live in, inhabit
* héeya = to be afraid
* thad = to be able
* sháad = to come, to go
* maradal = hole
* bath = claw
* -nal = case marker, suffix for manner.
* radama = to non-touch; avoiding touch; to actively refrain from touching
* rumad = to hide
* uná = leader
* mime = to ask
* lith = thought
* wo = imagined or invented by the speaker.
* dóhin = to create, to enact
* ladinime = to interact
* wilomina = to perform, act
* -á = doer
* doól = finally
* radimil = simple
* wo = imagined or invented by speaker; hypothetical; imagined or invented by X; hypothetical (evidence morpheme)
* dal = thing
* une = to wear
* wod = to sit
* dó- = cause-to-VERB prefix
* nin = to cause
* dush = to have to
* lo = to rejoice
* híya = to be small
* rahíya = to be big

**Notes**

No word in official dictionary for "plan" or "idea". I used "thought",
as well as "wodal", which I "invented", as a mix between wo and dal.

To make the cat wear a bell ... dóhune rul limlimeth

There also isn't an entry for "easy", radezh is "difficult". dezh doesn't have an entry.
Can I just use "dezh" as easy??
