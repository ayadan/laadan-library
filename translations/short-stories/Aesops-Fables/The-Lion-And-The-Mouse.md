# English: Belling the Cat

A Lion lay asleep in the forest, his great head resting on his paws. A timid little Mouse came upon him unexpectedly, and in her fright and haste to get away, ran across the Lion's nose. Roused from his nap, the Lion laid his huge paw angrily on the tiny creature to kill her.

"Spare me!" begged the poor Mouse. "Please let me go and some day I will surely repay you."
The Lion was much amused to think that a Mouse could ever help him. But he was generous and finally let the Mouse go.

Some days later, while stalking his prey in the forest, the Lion was caught in the toils of a hunter's net. Unable to free himself, he filled the forest with his angry roaring. The Mouse knew the voice and quickly found the Lion struggling in the net. Running to one of the great ropes that bound him, she gnawed it until it parted, and soon the Lion was free.

"You laughed when I said I would repay you," said the Mouse. "Now you see that even a Mouse can help a Lion."


# Láadan: ?


# Ido: La leono e la muso

**Translation by Tea Coba**

Olim esis dormanta leono qua kushis en la foresto kun sua granda kapo repozanta sur la pedi. Subite, mikra e timida muso venis a la leono e dum la muso esforcis eskapar pro pavoro, ol renkonris la naso del leono. Vekigita ek sua somno, la leono pozis furioze sua pedo sur la mikra bestio por ocidar ol.

"Lasez me vivar!" suplikis la kompatinda muso. "Lasez me irar ed ul-die me certe rekompensos tu"
La ideo de muso qua povus helpar leono ya esis amuza. Ma ol esis tre jenerosa e lasis la muson irar.

Pos kelke dii, dum ol marchis dop olua viktimo, la leono falis en la reti de kaptisto. Sen povar liberigar su, ol plenigis la foresto per olua furioza mujo. La muso rikonocis la voco e quik trovis la leono baraktanta en la reto. Kurante ad unu dil granda kordi per qua ol esas ligita, la muso derodis la kordo til ol ruptesis, e balde la leono esis libera..

"Tu ridis kande me dicis ke me rekompensus tu," dicis la muso. "Nun tu vidas ke mem muso povas helpar leono.




---

## Translation notes

### Word list


